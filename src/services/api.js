import axios from "axios";

export const api = axios.create({
    baseURL: "https://empresas.ioasys.com.br/api"

});

export const createSession = async (email, password) => {
    const response = await api.post("/v1/users/auth/sign_in", { email, password });
    return response;
}