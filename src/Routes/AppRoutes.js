import React, { useContext } from "react";
import { BrowserRouter as Router, Navigate, Route, Routes } from "react-router-dom";

import Enterprise from "../components/Enterprise";
import Login from "../components/Login";

import { AuthContext, AuthProvider } from "../context/auth";

const AppRoutes = () => {
    const Private = ({ children }) => {
        const { authenticated, } = useContext(AuthContext);


        return authenticated ? children : <Navigate to="/" />;
    }

    return (
        <Router>
            <AuthProvider>
                <Routes>
                    <Route exact path="/" element={<Login />} />
                    <Route exact path="/enterprise" element={<Private><Enterprise /></Private>} />
                </Routes>
            </AuthProvider>
        </Router>
    )
}

export default AppRoutes;