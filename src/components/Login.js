import React, { useState, useContext } from 'react';
import logo from '../assets/images/logo-home@2x.png';
import emaill from '../assets/images/ic-email@2x.png';
import passwordd from '../assets/images/ic-cadeado@2x.png'
import { AuthContext } from '../context/auth';

import '../styles/styles.css';
import { Button } from 'reactstrap';

import Spinner from './Spinner';

const Login = () => {
    const { isLogged } = useContext(AuthContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);

    const handleSubmit = async (event) => {
        event.preventDefault();
        setLoading(true);
        await isLogged(email, password);
        setLoading(false)
    }

    return (

        <div className="container-login">
            <div className="wrap-login">
                <form className="login-form" onSubmit={handleSubmit}>
                    <span className="login-form-title">
                        <img src={logo} alt="logo" width="90px" />
                        <span className="login-form-title"> BEM-VINDO AO EMPRESAS </span>
                        <span className="login-form-subtittle">Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</span>
                    </span>

                    <div className="wrap-input">
                        <input
                            className="focus-input"
                            type="email"
                            placeholder="E-mail"
                            value={email}
                            onChange={(event) => setEmail(event.target.value)}
                        />
                        <span>
                            <img src={emaill} alt="envelope" />
                        </span>
                    </div>
                    <div className="wrap-input">
                        <input
                            className="focus-input"
                            type="password"
                            placeholder="Senha"
                            value={password}
                            onChange={(event) => setPassword(event.target.value)}
                        />
                        <span>
                            <img src={passwordd} alt="cadeado" />
                        </span>
                    </div>
                    <Spinner loading={loading} />
                    <Button type="submit" color="success" size="lg" className="btn-login">
                        ENTRAR
                    </Button>
                </form>
            </div>
        </div>


    )
};

export default Login;
