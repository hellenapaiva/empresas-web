import React from 'react';
import { ClipLoader } from 'react-spinners';

const Spinner = (props) => {
    if (!props.loading) {
        return <div></div>
    }


    return (
        <div className="wrap-spinner">
            <div className="spinner"> <ClipLoader loading={props.loading} size={120} color="#57bbbc" /></div>
        </div>
    )
};

export default Spinner;
