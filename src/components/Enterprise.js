import React, { useContext, useState } from 'react';
import "../styles/styles.css";

import logoWhite from '../assets/images/logo-nav.png'
import search from '../assets/images/ic-search-copy.png'

import { Button } from 'reactstrap';

import { AuthContext } from '../context/auth';

const Enterprise = () => {
    const [showLogo, setShowLogo] = useState(true);
    const { logout } = useContext(AuthContext);


    const handleLogout = () => {
        logout();
    }

    return (

        <div >
            <div className="header-top">
                <input className={!showLogo ? "logoShow input-header" : "none input-header"} ></input>

                <img className={showLogo ? "logoShow logo-ioasys" : "none logo-ioasys"} src={logoWhite} alt="logo-ioasys" />
                <img className="search" src={search} alt="search" onClick={() => setShowLogo(!showLogo)} />

            </div>
            <div className="btn-logout">
                <Button color="danger" onClick={handleLogout} size="lg">Logout</Button>
            </div>
            <div>
                <h1 className="text-initial">Clique na busca para iniciar.</h1>
            </div>

        </div>

    );
};

export default Enterprise;
