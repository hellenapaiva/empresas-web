import React, { createContext, useEffect, useState } from "react";

import { useNavigate } from "react-router-dom";
import { createSession } from "../services/api";

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
    const navigate = useNavigate();
    const [user, setUser] = useState(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const recoveredUser = localStorage.getItem("user");
        if (recoveredUser) {
            setUser(JSON.parse(recoveredUser));
        }
        setLoading(false);
    }, [])

    const isLogged = async (email, password) => {

        const response = await createSession(email, password);

        const { data: loggedUser, headers } = response;


        localStorage.setItem("user", JSON.stringify(loggedUser));
        localStorage.setItem("token", headers["access-token"]);
        localStorage.setItem("token", headers["client"]);
        localStorage.setItem("token", headers["uid"]);

        setUser(loggedUser);
        navigate("/enterprise");

    }

    const logout = () => {
        localStorage.removeItem("user");
        setUser(null);
        navigate("/");
    }

    return (

        <AuthContext.Provider value={{ authenticated: !!user, user, loading, isLogged, logout }}>
            {children}
        </AuthContext.Provider>
    )

}